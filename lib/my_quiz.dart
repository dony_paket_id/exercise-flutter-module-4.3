import 'dart:math';

import 'package:flutter/material.dart';

class TileInfo {
  String title;
  Color color;
  TileInfo({
    required this.title,
    this.color = Colors.purple,
  });
}

int score = 0;

class MyQuiz extends StatefulWidget {
  @override
  State<MyQuiz> createState() => _MyQuizState();
}

class _MyQuizState extends State<MyQuiz> {
  // data ini tidak akan pernah di eksekusi saat hot reload
  int currentPage = 0;
  List<bool?> boolCheckList = List.generate(10, (index) => Random().nextBool());
  List<bool> boolToShow = List.generate(10, (index) => Random().nextBool());

  @override
  Widget build(BuildContext context) {
    List<TileInfo> tileInfoList = [
      TileInfo(title: "Red", color: Colors.red),
      TileInfo(title: "Blue", color: Colors.blue),
      TileInfo(title: "Green", color: Colors.green),
      TileInfo(title: "Orange", color: Colors.orange),
    ];

    return Scaffold(
      appBar: AppBar(title: const Text("My Quiz")),
      body: currentPage == 0
          ? quiz1(tileInfoList)
          : currentPage == 1
              ? quiz2()
              : scorePage(),
    );
  }

  Widget quiz1(List<TileInfo> tileInfoList) => Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/polar-33.png"),
            opacity: 0.5,
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.white.withOpacity(0),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Select the red square",
                style: TextStyle(fontSize: 25.0),
              ),
              Spacer(),
              ...List.generate(
                tileInfoList.length,
                (index) => _customListTile(
                  title: tileInfoList[index].title,
                  color: tileInfoList[index].color,
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      );

  Widget quiz2() => SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Check Only YES Tile",
                style: TextStyle(fontSize: 25.0),
              ),
              ...List.generate(
                boolCheckList.length,
                (index) => CheckboxListTile(
                  activeColor: boolToShow[index] == boolCheckList[index]
                      ? Colors.green
                      : Colors.red,
                  tileColor: boolToShow[index] == boolCheckList[index]
                      ? Colors.green
                      : Colors.red,
                  title: Text(boolToShow[index] ? "YES" : "NO"),
                  value: boolCheckList[index],
                  onChanged: (valueBool) {
                    setState(() {
                      boolCheckList[index] = valueBool;
                    });
                  },
                ),
              ),
              TextButton(
                onPressed: () {
                  bool everythingIsFine = true;

                  for (int i = 0; i < boolCheckList.length; i++) {
                    if (boolToShow[i] != boolCheckList[i]) {
                      everythingIsFine = false;
                    }
                  }

                  if (everythingIsFine) {
                    score = score + 1;
                  }

                  setState(() {
                    currentPage = currentPage + 1;
                  });
                },
                child: const Text("Finish"),
              ),
            ],
          ),
        ),
      );

  Widget scorePage() => Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("images/fauna-animal-teamwork.png"),
            SizedBox(height: 20.0),
            Text(
              "Score $score",
              style: const TextStyle(fontSize: 30.0),
            ),
          ],
        ),
      );

  Widget _customListTile({
    required String title,
    required Color color,
  }) =>
      ListTile(
        title: Text(title),
        tileColor: color,
        onTap: () {
          if (color == Colors.red) {
            score = 1;
          } else {
            score = 0;
          }

          setState(() {
            currentPage = 1;
          });
        },
      );
}
